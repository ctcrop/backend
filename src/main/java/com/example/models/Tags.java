package com.example.models;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PostLoad;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
public class Tags extends PanacheEntity {
    @Column(length = 50)
    private String label;
}

