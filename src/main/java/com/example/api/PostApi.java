package com.example.api;

import com.example.models.Posts;

import javax.enterprise.context.RequestScoped;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import java.util.List;

@Consumes("application/json")
@Produces("application/json")
@Path("api/post")
@RequestScoped
public class PostApi {
    @POST
    @Transactional
    public Posts post(Posts body) {
        return cuPosts(body);
    }

    @PUT
    @Transactional
    public Posts put(Posts body) {
        return cuPosts(body);
    }


    @DELETE
    @Transactional
    @Path("/{id}")
    public void delete(@PathParam("id") Long id) {
        var posts = Posts.findById(id);

        if (posts != null) {
            posts.delete();
        }
    }

    @GET
    @Path("/{id}")
    public Posts get(@PathParam("id") Long id) {
        return Posts.findById(id);
    }

    @GET
    public List<Posts> get() {
        return Posts.findAll().list();
    }

    private static Posts cuPosts(Posts body) {
        var posts = new Posts();

        if (body.id != null) {
            posts = Posts.findById(body.id);
        }

        posts.setTags(body.getTags());
        posts.setTitle(body.getTitle());
        posts.setContent(body.getContent());

        posts.persist();
        return posts;
    }
}
