package com.example.api;

import com.example.models.Tags;

import javax.enterprise.context.RequestScoped;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import java.util.List;

@Consumes("application/json")
@Produces("application/json")
@Path("api/tag")
@RequestScoped
public class TagApi {
    @POST
    @Transactional
    public Tags post(Tags body) {
        return cuTags(body);
    }

    @PUT
    @Transactional
    public Tags put(Tags body) {
        return cuTags(body);
    }

    @DELETE
    @Transactional
    @Path("/{id}")
    public void delete(@PathParam("id") Long id) {
        var tags = Tags.findById(id);

        if (tags != null) {
            tags.delete();
        }
    }

    @GET
    @Path("/{id}")
    public Tags get(@PathParam("id") Long id) {
        return Tags.findById(id);
    }

    @GET
    public List<Tags> get() {
        return Tags.findAll().list();
    }

    private static Tags cuTags(Tags body) {
        var tags = new Tags();

        if (body.id != null) {
            tags = tags.findById(body.id);
        }

        tags.setLabel(body.getLabel());
        tags.persist();

        return tags;
    }
}
